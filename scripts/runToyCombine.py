#!/usr/bin/env python

import os

for toy in range(1, 200+1):

  print "Submitting limit computation for pseudo-experiment %i..."%toy
  cmd = "./scripts/runCombine.py -M MarkovChainMC -d datacards_Bayesian -f gg --mass 750 --noSyst -o logs_Bayesian_noSyst --postfix PE%i --condor"%toy
  #cmd = "./scripts/runCombine.py -M MarkovChainMC -d datacards_Bayesian -f gg --mass 750 -o logs_Bayesian --postfix PE%i --condor"%toy
  #cmd = "./scripts/runCombine.py -M MarkovChainMC -d datacards_Bayesian_decoBkg -f gg --mass 750 -o logs_Bayesian_decoBkg --postfix PE%i --condor"%toy
  #print cmd
  os.system(cmd)