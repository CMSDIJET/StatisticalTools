#!/usr/bin/env python

import os, sys, re, glob
from ROOT import TMath
from array import array


#######################################################
def getQuantiles(limits, median, onesigma, twosigma):

    nit = len(limits)
    if nit==0:
        return

    # sort the list with limits
    limits = sorted(limits)
    limits_array = array('d',limits)

    # median for the expected limit
    median[0] = TMath.Median(nit, limits_array)

    # quantiles for the expected limit bands
    prob = array('d',[]) # array with quantile boundaries
    prob.append(0.021)
    prob.append(0.159)
    prob.append(0.841)
    prob.append(0.979)

    # array for the results

    quantiles = array('d',[0., 0., 0., 0.])
    TMath.Quantiles(nit, 4, limits_array, quantiles, prob) # evaluate quantiles

    onesigma[0] = quantiles[1]
    onesigma[1] = quantiles[2]
    twosigma[0] = quantiles[0]
    twosigma[1] = quantiles[3]

    return
#######################################################


def main():

    res_type = sys.argv[1]
    masses = range(int(sys.argv[2]),int(sys.argv[3])+int(sys.argv[4]),int(sys.argv[4]))

    for mass in masses:

        observed_bound = 0.

        log_file = open('limits_MarkovChainMC_' + res_type + '_m' + str(mass) + '.log','r')
        outputlines = log_file.readlines()
        log_file.close()

        foundMethod = False

        # read the log file
        for line in outputlines:
            if re.search('^ -- MarkovChainMC --', line):
                foundMethod = True
            if re.search("^Limit: r", line) and foundMethod:
                observed_bound = float(line.split()[3])


        expected_upper_bounds = []
        pwd = os.getcwd()
        logs = glob.glob(os.path.join(pwd,"*.log"))

        job_numbers = []
        for log in logs:
            if not re.search('limits_MarkovChainMC_' + res_type + '_m' + str(mass) + '_PE' + '\d+.log$', log): continue
            job_numbers.append(int(log.replace('.log','').split('_')[-1].replace('PE','')))

        job_numbers = sorted(job_numbers)

        for num in job_numbers:
            log_file = open('limits_MarkovChainMC_' + res_type + '_m' + str(mass) + '_PE' + str(num) + '.log','r')
            outputlines = log_file.readlines()
            log_file.close()

            foundMethod = False

            # read the log file
            for line in outputlines:
                if re.search('^ -- MarkovChainMC --', line):
                    foundMethod = True
                if re.search("^Limit: r", line) and foundMethod:
                    expected_upper_bounds.append(float(line.split()[3]))

        #print observed_bound
        #print expected_upper_bounds

        median = [0.]
        onesigma = [0., 0.]
        twosigma = [0., 0.]

        getQuantiles(expected_upper_bounds, median, onesigma, twosigma)

        print 'Limits for ' + res_type + ' resonance at m = ' + str(mass) + ' GeV'
        print ''
        print 'observed bound = [ 0 , ' + str(observed_bound) + ' ]'
        print ''
        print '***** expected upper bounds *****'
        print 'median: ' + str(median[0])
        print '+/-1 sigma band: [ ' + str(onesigma[0]) + ' , ' + str(onesigma[1]) + ' ]'
        print '+/-2 sigma band: [ ' + str(twosigma[0]) + ' , ' + str(twosigma[1]) + ' ]'


if __name__ == '__main__':
    main()
