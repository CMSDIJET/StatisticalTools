#!/usr/bin/env python

import os

for toy in range(1, 200+1):

  print "Processing pseudo-experiment %i..."%toy
  cmd = "./scripts/createDatacards.py --inputData inputs/higgsCombine_gg_m750_toys.root --dataHistname h_toy_%i__mjj --inputSig inputs/ResonanceShapes_gg_13TeV_CaloScouting_Spring15.root -f gg -o datacards_Bayesian -l 1918 --lumiUnc 0.027 --jesUnc 0.05 --jerUnc 0.04 --mass 750 --runFit --p1 7.2345e+00 --p2 6.7331e+00 --p3 3.0517e-01 --massMin 453 --massMax 2037 --fitStrategy 2 --fixBkg --postfix PE%i"%(toy,toy)
  #cmd = "./scripts/createDatacards.py --inputData inputs/higgsCombine_gg_m750_toys.root --dataHistname h_toy_%i__mjj --inputSig inputs/ResonanceShapes_gg_13TeV_CaloScouting_Spring15.root -f gg -o datacards_Bayesian_decoBkg -l 1918 --lumiUnc 0.027 --jesUnc 0.05 --jerUnc 0.04 --mass 750 --runFit --p1 7.2345e+00 --p2 6.7331e+00 --p3 3.0517e-01 --massMin 453 --massMax 2037 --fitStrategy 2 --decoBkg --postfix PE%i"%(toy,toy)
  #print cmd
  os.system(cmd)